package cr.ac.ucr.laboratorio2_android;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import adapters.StudentsListAdapter;
import models.Student;

public class StudentsListActivity extends AppCompatActivity {

    private RecyclerView students_list_recycler;
    private List<Student> studentList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_list);

        students_list_recycler = findViewById(R.id.students_list_recycler);
        studentList = getStudents();

        StudentsListAdapter adapter = new StudentsListAdapter(studentList, this);
        students_list_recycler.setLayoutManager(new LinearLayoutManager(this));
        students_list_recycler.setAdapter(adapter);

    }

    //Conexión a BD o consumir WebAPI/WebService
    private List<Student> getStudents() {
        List<Student> tempList = new ArrayList<>();
        tempList.add(new Student("B00000", "Jorge", "Román"));
        tempList.add(new Student("B11111", "María", "Solano"));
        tempList.add(new Student("B22222", "Carlos", "Gutierrez"));
        tempList.add(new Student("B33333", "Juan", "Guillén"));
        tempList.add(new Student("B44444", "Pedro", "Naranjo"));
        tempList.add(new Student("B55555", "Michelle", "Pereira"));
        tempList.add(new Student("B66666", "Andrea", "Quirós"));
        tempList.add(new Student("B77777", "Sonia", "Rojas"));

        return tempList;
    }


}
